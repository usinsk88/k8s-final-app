#!/bin/bash

folder_name=""
cloud_id=""

# Удаляем предыдущий kubectl конфиг
rm ~/.kube/config

#Настраиваем локальный kubectl
yc managed-kubernetes cluster get-credentials --name k8sfinal --folder-name=$folder_name --external --cloud-id=$cloud_id

#yc managed-kubernetes cluster list
#echo "Print CLUSTER_ID "
#read CLUSTER_ID

#Устанавливаем CLUSTER_ID
CLUSTER_ID=$(yc managed-kubernetes cluster list --folder-name=$folder_name --cloud-id=$cloud_id | grep -oP [a-z0-9]{20})

# Создаём ca.pem
yc managed-kubernetes cluster get --id $CLUSTER_ID --format json | jq -r .master.master_auth.cluster_ca_certificate | awk '{gsub(/\\n/,"\n")}1' > ca.pem

# Создаём ServiceAccount в кластере
kubectl create -f sa.yaml

# Записывает токен в переменную SA_TOKEN
SA_TOKEN=$(kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') -o json | jq -r .data.token | base64 --d)

# Записываем IP-адрес кластера в переменную MASTER_ENDPOINT
MASTER_ENDPOINT=$(yc managed-kubernetes cluster get --id $CLUSTER_ID --format json | jq -r .master.endpoints.external_v4_endpoint)

# Добавляем сведения о кластере Kubernetes в файл конфигурации
kubectl config set-cluster k8sfinal --certificate-authority=ca.pem --server=$MASTER_ENDPOINT --kubeconfig=config

# Добавляем информацию о токене для admin-user в файл конфигурации
kubectl config set-credentials admin-user --token=$SA_TOKEN --kubeconfig=config

# Добавляем информацию о контексте в файл конфигурации
kubectl config set-context default --cluster=k8sfinal --user=admin-user --kubeconfig=config

# Применяем созданный конфиг
kubectl config use-context default --kubeconfig=config
