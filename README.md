# Информация по использованию проекта.

Данный проект собирает Docker-образ приложения loadgenerator и загружает его в реджестри. Затем этот образ с помощью helm-чарта запускается в k8s-кластере. Также в кластере запускается [demo-приложение](https://github.com/GoogleCloudPlatform/microservices-demo).
K8s-кластер можно развернуть в Яндекс Облаке с помощью terraform манифестов из каталога [manifest](manifest).

## Запуск проекта

> На управляющем компьютере должны быть установлены yc, kubectl, helm, jq. Интерфейс командной строки Yandex Cloud должен быть [инициализированн](https://cloud.yandex.ru/docs/cli/quickstart).

Скрипты выполняется на управляющем компьютере. Скрипт ya.sh настраивает kubectl на управляющем компьютере и создаёт файлы статической конфигурации kubectl для gitlab runner.
1. В скрипте [ya.sh](ya.sh) задаём переменные:
    - folder_name - имя каталога в котором развёрнут Managed Service for Kubernetes.
    - cloud_id - идентификатор яндекс облака. [Как получить](https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id).
2. Выполняем скрипт.
3. Полученные файлы config и ca.pem необходимо загрузить в [Secure Files](https://docs.gitlab.com/ee/ci/secure_files/index.html#add-a-secure-file-to-a-project) репозитория.
4. Выполняем скрипт [config.sh](config.sh), который устанавливает в кластер ingress nginx controller, Prometheus, Grafana, Loki и создаёт ingress правило для Grafana.
5. В консоли управления Яндекс облако создаём ресурсные DNS записи k8sfinal.ru и grafana.k8sfinal.ru с указанием адреса созданного Load balancer.
6. Создать [переменные](https://docs.gitlab.com/ee/ci/variables/index.html#define-a-cicd-variable-in-the-ui) проекта:
    - CI_REGISTRY - адрес реджестри (registry.gitlab.com).
    - CI_USER - имя пользователя от реджестри.
    - CI_PASSWORD - пароль от реджестри.
7. Выполнить git push в ветку main для запуска gitlab CI. (Сборка образа произойдёт только при изменениях в папке [loadgenerator/app/](loadgenerator/app/))
