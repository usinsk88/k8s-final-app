#!/bin/bash

# Устанавливаем ingress nginx
helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set controller.metrics.enabled=true \
 --set-string controller.podAnnotations."prometheus\.io/scrape"="true" --set-string controller.podAnnotations."prometheus\.io/port"="10254"

# Ждём пока запустятся все поды ingress
kubectl wait --namespace=ingress-nginx --for=condition=ContainersReady pods --all --timeout=600s

# Устанавливаем prometheus и grafana
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
kubectl create ns monitoring
helm install stable prometheus-community/kube-prometheus-stack -n monitoring
#default login\pass: admin\prom-operator

# Применяем манифест Ingress Grafana
kubectl apply -f grafana-ing.yml

# Устанавливаем cert-manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.0/cert-manager.yaml

# Настраиваем Let's Encrypt Issuer
kubectl apply -f clusterIssuer.yml

# Устанавливаем Loki
helm upgrade --install -n monitoring loki-stack grafana/loki-stack --set fluent-bit.enabled=true,promtail.enabled=false
