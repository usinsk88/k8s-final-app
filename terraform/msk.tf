# Network
resource "yandex_vpc_network" "k8s-network" {
  name = "k8s-network"
}

resource "yandex_vpc_subnet" "k8s-subnet-a" {
  name           = "k8s-subnet-a"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.k8s-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  depends_on = [
    yandex_vpc_network.k8s-network,
  ]
}

resource "yandex_vpc_subnet" "k8s-subnet-b" {
  name           = "k8s-subnet-b"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.k8s-network.id
  v4_cidr_blocks = ["192.168.20.0/24"]
  depends_on = [
    yandex_vpc_network.k8s-network,
  ]
}

resource "yandex_vpc_subnet" "k8s-subnet-c" {
  name           = "k8s-subnet-c"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.k8s-network.id
  v4_cidr_blocks = ["192.168.30.0/24"]
  depends_on = [
    yandex_vpc_network.k8s-network,
  ]
}

# KMS
resource "yandex_kms_symmetric_key" "k8s-final-key" {
  name              = "k8s-final-key"
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
}

# Security group
resource "yandex_vpc_security_group" "k8s-main-sg" {
  name        = "k8s-main-sg"
  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените её к кластеру и группам узлов."
  network_id  = yandex_vpc_network.k8s-network.id

  ingress {
    protocol          = "TCP"
    description       = "Правило разрешает проверки доступности c диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."
    predefined_target = "loadbalancer_healthchecks"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol          = "ANY"
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "Правило разрешает взаимодействие под-под и сервис-сервис."
    v4_cidr_blocks = ["10.112.0.0/16","10.96.0.0/16"]
    from_port      = 0
    to_port        = 65535
  }

  egress {
    protocol       = "ANY"
    description    = "Правило разрешает весь исходящий трафик. Узлы могут связаться c Yandex Container Registry, Object Storage, Docker Hub и т. д."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "k8s-public-services" {
  name        = "k8s-public-services"
  description = "Правила группы разрешают подключение к сервисам из интернета. Примените правила только для групп узлов."
  network_id  = yandex_vpc_network.k8s-network.id

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 30000
    to_port        = 32767
  }
}

resource "yandex_vpc_security_group" "k8s-nodes-ssh-access" {
  name        = "k8s-nodes-ssh-access"
  description = "Правила группы разрешают подключение к узлам кластера по SSH. Примените правила только для групп узлов."
  network_id  = yandex_vpc_network.k8s-network.id

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает подключение к узлам по SSH."
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
}

resource "yandex_vpc_security_group" "k8s-master-whitelist" {
  name        = "k8s-master-whitelist"
  description = "Правила группы разрешают доступ к API Kubernetes из интернета. Примените правила только к кластеру."
  network_id  = yandex_vpc_network.k8s-network.id

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает подключение к API Kubernetes через порт 6443."
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 6443
  }

  ingress {
    protocol       = "TCP"
    description    = "Правило разрешает подключение к API Kubernetes через порт 443."
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }
}

# Cluster creating
# resource "yandex_kubernetes_cluster" "k8sfinal" {
#   name        = "k8sfinal"
#   description = "Cluster for final work"

#   network_id = yandex_vpc_network.k8s-network.id

#   master {
#     version = "1.23"
#     zonal {
#       zone      = "ru-central1-a"
#       subnet_id = yandex_vpc_subnet.k8s-subnet-1.id
#     }

#     public_ip = true

#     security_group_ids = [
#       yandex_vpc_security_group.k8s-main-sg.id,
#       yandex_vpc_security_group.k8s-master-whitelist.id
#     ]

#     maintenance_policy {
#       auto_upgrade = false
#     }
#   }

#   service_account_id      = "${yandex_iam_service_account.folderEditor.id}"
#   node_service_account_id = "${yandex_iam_service_account.k8s-node-sa.id}"

#   release_channel = "STABLE"
#   network_policy_provider = "CALICO"

#   kms_provider {
#     key_id = yandex_kms_symmetric_key.k8s-final-key.id
#   }
# }




resource "yandex_kubernetes_cluster" "k8sfinal" {
  name        = "k8sfinal"
  description = "Cluster for final work"

  network_id = yandex_vpc_network.k8s-network.id

  master {
    version = "1.23"
    regional {
      region = "ru-central1"

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-a.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-a.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-b.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-b.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-c.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-c.id}"
      }
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = false
    }
  }

  service_account_id      = "${yandex_iam_service_account.folderEditor.id}"
  node_service_account_id = "${yandex_iam_service_account.k8s-node-sa.id}"

  release_channel = "STABLE"
  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s-final-key.id
  }
}








# Workers node
resource "yandex_kubernetes_node_group" "k8s_workers" {
  cluster_id  = yandex_kubernetes_cluster.k8sfinal.id
  name        = "workers"
  description = "description"
  version     = "1.23"

  instance_template {
    name = "worker-{instance.index}"
    platform_id = "standard-v2"
    # network_interface {
    #   nat                = true
    #   subnet_ids         = [yandex_vpc_subnet.k8s-subnet-1.id]
    #   security_group_ids = [
    #     yandex_vpc_security_group.k8s-main-sg.id,
    #     yandex_vpc_security_group.k8s-nodes-ssh-access.id,
    #     yandex_vpc_security_group.k8s-public-services.id
    #   ]
    # }

    network_interface {
      subnet_ids = [
        yandex_vpc_subnet.k8s-subnet-a.id,
        yandex_vpc_subnet.k8s-subnet-b.id,
        yandex_vpc_subnet.k8s-subnet-c.id,
      ]
      nat = true
    }

    metadata = {
      ssh-keys = "kube:${file("files/id_rsa.pub")}"
    }

    resources {
      memory = 4
      cores  = 4
      core_fraction = 5
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }

    location {
      zone = "ru-central1-b"
    }

    location {
      zone = "ru-central1-c"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }
}

#Создаём публичную зону DNS
resource "yandex_dns_zone" "zone1" {
  name    = "k8sfinalru"
  zone    = "k8sfinal.ru."
  public  = true
}
