resource "yandex_iam_service_account" "folderEditor" {
  name        = "folder-editor"
  folder_id   = var.yc_folder_id
}

resource "yandex_resourcemanager_folder_iam_binding" "folderEditor" {
  folder_id   = var.yc_folder_id
  role        = "editor"
  members     = [
    "serviceAccount:${yandex_iam_service_account.folderEditor.id}",
  ]
}

resource "yandex_iam_service_account" "k8s-node-sa" {
  name        = "k8s-node-sa"
  folder_id   = var.yc_folder_id
}

resource "yandex_resourcemanager_folder_iam_binding" "k8s-node-sa" {
  folder_id   = var.yc_folder_id
  role        = "container-registry.images.puller"
  members     = [
    "serviceAccount:${yandex_iam_service_account.k8s-node-sa.id}",
  ]
}
