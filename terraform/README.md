# Создание кластера k8s в Яндекс Облако

> terraform и провайдер yandex.cloud ([Инструкция](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform) по установке)

## Создание инфраструктуры на yandex.cloud:
1. В файле "terraformvars.tf" укажите:

    1. yc_token = "< token >" - [OAuth-токен](https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token) для доступа к Yandex Cloud.
    2. yc_cloud_id = "< cloud_id >" - Идентификатор облака, в котором Terraform создаст ресурсы [Как получить](https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id).
    3. yc_folder_id = "< folder_id >" [Идентификатор папки](https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id), в которой будут созданы ресурсы.

2. Перейдите в каталог "./terraform" и выполните команду "`terraform apply --auto-approve`".
При этом будут созданы:

    1. Региональный кластер k8s с 3 мастер нодами и настроенный firewall
    2. Группа узлов worker нод
    3. DNS зона k8sfinal.ru
