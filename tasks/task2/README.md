1. Скриншот сервиса [frontend](frontend.png)
2. Папка helm чарт приложения [loadgenerator](../../loadgenerator/helm/) и [configmap](../../loadgenerator/helm/templates/configmap.yaml)
3. Скриншот шага [build](build.png)
4. Скриншот шага [deploy](deploy.png)
5. Скриншот хранение [kubeconfig](kubeconfig in secure files.png). kubeconfig создаётся при помощи скрипта [ya.sh](../../ya.sh) и загружается вручную в secure files репозитория.